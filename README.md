Access generator
================

Introduction
------------

Generate token via security manager given address/login

Use
---

```bash
./accessgenerator "127.0.0.1" "GhostButler2018Admin" "GhostButlerProject2018*"
```

Example
-------

```bash
curl -k -H "X-Ghost-Butler-Key: `accessgenerator "127.0.0.1" "GhostButler2018Admin" "GhostButlerProject2018*"`" https://127.0.0.1:16562/api/v1/controller/outlets/
```

Author
------

SOARES Lucas <lucas.soares.npro@gmail.com>

https://gitlab.com/ghostbutler/tool/accessgenerator.git
