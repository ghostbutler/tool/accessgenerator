#!/bin/bash

# check root
if [ "$EUID" -ne 0 ]; then
    echo "you are not running script as root, exiting"
    exit 1
fi

# install
echo "building..."
go build -o tmp
echo "copy executable into /usr/bin"
cp tmp /usr/bin/accessgenerator
echo "remove temporary file"
rm tmp
