package main

import (
	"fmt"
	"gitlab.com/ghostbutler/tool/service/keystore"
	"os"
)

// entry point
func main( ) {
	// extract data
	if len( os.Args ) >= 4 {
		if key, err := keystore.Login( os.Args[ 2 ], os.Args[ 3 ], os.Args[ 1 ], "test" ); err == nil {
			if key, err := keystore.RequestOneTimeKey( key,
				os.Args[ 1 ] ); err == nil {
				fmt.Println( key.PublicKey )
			} else {
				fmt.Println( "couldn't request one time key" )
			}
		} else {
			fmt.Println( "couldn't request api key" )
		}
	} else {
		fmt.Println( "usage: ./accessgenerator address username password" )
	}
}
